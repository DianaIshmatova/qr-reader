const gulp = require('gulp');
const fs = require('fs');

gulp.task('cc', () => {
  const name = process.argv[3]
    .replace('--', '')
    .replace(/^\w/, f => f.toUpperCase());

  const prefix = './src/components/';
  fs.mkdir(`${prefix}${name}`, () => {
    const js = fs.createWriteStream(`${prefix}${name}//${name}.jsx`);
    js.write(`import React, { PropTypes, PureComponent } from 'react';
import styles from './${name}.pcss';
import classNames from 'classnames/bind';
const cx = classNames.bind(styles);

export default class ${name} extends PureComponent {
  static propTypes = {

  }

  render() {
    return (<div></div>)
  }
}`);
    js.end();
    const pcss = fs.createWriteStream(`${prefix}${name}\\${name}.pcss`);
    pcss.end();
  });
});
