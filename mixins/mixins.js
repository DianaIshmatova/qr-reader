const postcss = require('postcss');

module.exports = {
  shadow: (mixin, r, g, b, opacity, angle, distance, size, spread, inset) => {
    const color = `rgba(${r}, ${g}, ${b}, ${opacity / 100})`;
    const yShadow = Math.cos(Math.PI - Math.PI * angle / 180).toFixed(0) * distance;
    const xShadow = Math.sin(Math.PI * angle / 180).toFixed(0) * distance;
    const spreadShadow = (size * spread / 100);
    const blurShadow = size - spread;
    const insetShadow = inset ? 'inset' : '';
    const result = `${yShadow}px ${xShadow}px ${blurShadow}px ${spreadShadow}px ${color} ${insetShadow}`;
    const decl = postcss.decl({ prop: 'box-shadow', value: result });
    mixin.replaceWith(decl);
  },
};
