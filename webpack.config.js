const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const fs = require('fs');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const NODE_ENV = process.env.NODE_ENV || 'development';
/*comment*/

const getEnvPlugins = () => {
  const commopPlugins = [
    new webpack.EnvironmentPlugin({
      NODE_ENV: JSON.stringify(NODE_ENV)
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'index.html'),
    }),
  ];

  const devPlugins = [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: module => (
        module.context && module.context.indexOf('node_modules') !== -1
      ),
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest',
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ];

  const prodPlugins = [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: module =>
        (module.context && module.context.indexOf('node_modules') !== -1),
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest',
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: true,
    }),
    new ExtractTextPlugin({
      ignoreOrder: true,
      filename: '[name].css',
    }),
  ];

  const serverPlugins = [
    new ExtractTextPlugin({
      ignoreOrder: true,
      filename: '[name].css',
    }),
  ];

  switch (process.env.NODE_ENV) {
    case 'development':
      return commopPlugins.concat(devPlugins);
    case 'production':
      return commopPlugins.concat(prodPlugins);
    case 'server':
      return commopPlugins.concat(serverPlugins);
    default:
      return commopPlugins.concat(devPlugins);
  }
};

const generateEntries = () => {
  const pathToPages = path.resolve(__dirname, 'src', 'pages');
  const files = fs.readdirSync(pathToPages);
  if (process.env.NODE_ENV === 'server') {
    const serverEntry = files.reduce((result, file) => {
      if (fs.statSync(path.join(pathToPages, file)).isDirectory()) {
        result.push(path.resolve(pathToPages, file, file));
      }
      return result;
    }, []);

    return { all: serverEntry };
  }
  const entries = files.reduce((result, file) => {
    if (fs.statSync(path.join(pathToPages, file)).isDirectory()) {
      result[file] = path.resolve(pathToPages, file, file);
    }

    return result;
  }, {});

  if (process.env.NODE_ENV === 'development') {
    const hmr = ['react-hot-loader/patch',
      // activate HMR for React

      'webpack-dev-server/client?http://localhost:8080',
      // bundle the client for webpack-dev-server
      // and connect to the provided endpoint

      'webpack/hot/dev-server',
      // bundle the client for hot reloading
    ];

    entries.hmr = hmr;
  }

  return entries;
};

const getCssRule = () => {
  const devRule = [
    { loader: 'style-loader' },
    {
      loader: 'css-loader',
      options: {
        importLoaders: 1,
        modules: true,
        localIdentName: '[name]_[local]_[hash:base64:4]',
      },
    },
    { loader: 'postcss-loader' },
    { loader: path.resolve('loaders/b2bThemeLoader') },
  ];

  const prodRule = ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [{
      loader: 'css-loader',
      options: {
        importLoaders: 1,
        modules: true,
        localIdentName: '[name]_[local]_[hash:base64:4]',
      },
    },
      { loader: 'postcss-loader' },
      { loader: path.resolve('loaders/b2bThemeLoader') },
    ],
  });

  return process.env.NODE_ENV === 'development' ? devRule : prodRule;
};

module.exports = {
  context: path.resolve(__dirname, 'src'),
  entry: generateEntries,
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].js',
  },

 /* watch: true,

  watchOptions: {
    aggregateTimeout: 100
  },*/

  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      store: path.resolve(__dirname, './src/store/store'),
      utils: path.resolve(__dirname, './src/utils'),
      Header: path.resolve(__dirname, './src/pages/MobileCompany/components/Header/index.jsx'),
    },
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: [{
          loader: 'babel-loader',
        }],
        include: [
          path.resolve(__dirname, 'src'),
          /beeline|adaptive-components/,
        ],
      },
      {
        test: /\.p?css$/,
        use: getCssRule(),
      },

      {
        test: /\.(woff|woff2|ttf|png|jpg)$/,
        loader: 'url-loader',
      },

      {
        test: /\.svg$/,
        use: [
          { loader: 'babel-loader' },
          {
            loader: 'react-svg-loader',
          },
        ],
      },
    ],
  },

  plugins: getEnvPlugins(),

  devtool: process.env.NODE_ENV === 'development' ? 'source-map' : false,

  devServer: {
    headers: { 'Access-Control-Allow-Origin': '*' },
    stats: {
      colors: true,
      chunks: false,
      timings: true,
      chunkModules: false,
      modules: false,
      reasons: false,
    },
    historyApiFallback: true,
    hot: true,
    proxy: {
      '/api': 'http://localhost:9000',
      '/images': 'http://localhost:9000',
    },
  },
};
