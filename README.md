## B2B Frontend

### Installation
```npm i```

### Build
1. development ```npm start```
2. production ```npm run build```

### Development
1. Build application and navigate to localhost:8080

### Structure
1. pages
2. store - redux store lives here
3. styles - common styles
4. mixins - css mixins folder
5. utils - small helpers
6. vendors - jquery folder :)
7. entry.jsx - deprecated
9. tools - build tools

### Rules
1. Dont ignore linter errors :)
2. Commit format : Verb + description. For example: ```Add my super feature to mobile components```
3. Naming: 
   * for status and states: V3 like 'opened', 'linked'
   * noun for simple variables
   * modules starts from capital latter. Module name = Module index file name

### Env variables 
* NODE_ENV=production - minified client conde
* NODE_ENV=server - server code
* NODE_ENV=development - local development code with source maps

### Servers
webpack-dev-server serves assets for local development (port 8080) 
navigate to localhost:8080/:ComponentName
node js server is used for api emulation (port 9000)

### Production build files
Every index module in pages directory is compiled to separate script file.
All common chunks lives in common.js file



