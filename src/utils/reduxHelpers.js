const simpleMutator = (state, action) => action.payload;

export const simpleReducer = (actionType, initialState = {}, mutator = simpleMutator) =>
  (state = initialState, action) => {
    if (action.type === actionType) {
      return mutator(state, action);
    }

    return state;
  };
