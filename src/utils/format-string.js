export function formatPrice(input) {
  if (typeof input !== 'string') return input;

  return input.replace('руб.', '₽');
}

export function maskPhone(value) {
  let mask = '+7 (___) ___-__-__';
  const onlyDigits = value.replace(/\+7|\D/g, '');

  let lastDigit;
  onlyDigits.split('').forEach((digit) => {
    if (mask.indexOf('_') > 0) {
      mask = mask.replace('_', digit);
      lastDigit = digit;
    }
  });

  return mask.slice(0, mask.lastIndexOf(lastDigit) + 1);
}

export function maskContract(value) {
  return value.replace(/\D/g, '').substr(0, 10);
}

function declension(nounArray, number) {
  const cases = [2, 0, 1, 1, 1, 2];
  const index = (number % 100 > 4 && number % 100 < 20)
    ? 2
    : cases[(number % 10 < 5) ? number % 10 : 5];
  return nounArray[index];
}

const dictionary = {
  goods: ['товар', 'товара', 'товаров'],
};

export function translate(word, number) {
  if (!dictionary[word]) return null;
  return declension(dictionary[word], number);
}

export function formatPriceNumber(price) {
  if (price >= 10000) {
    return (String(price)).replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
  }

  return String(price);
}
