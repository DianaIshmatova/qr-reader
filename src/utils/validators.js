export const phone = value => ({
  status: (value && value.length === 10) ? 'ok' : 'fail',
  msg: 'Неверный формат номера телефона',
});

export const maskedPhone = value => ({
  status: /\+7 \(\d{3}\) \d{3}-\d{2}-\d{2}/.test(value) ? 'ok' : 'fail',
  msg: 'Неверный формат номера телефона',
});

export const email = (value) => {
  if (value === '') return { status: 'ok' };
  return {
    // form emailregex.com
    status: /^[-a-z0-9~!$%^&*_=+}{'?]+(\.[-a-z0-9~!$%^&*_=+}{'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i.test(value) ?
      'ok' : 'fail',
    msg: 'Неверный формат почты',
  };
};

export const sms = value => ({
  status: value.length > 3 ?
    'ok' : 'fail',
  msg: 'Неверный формат sms кода',
});

export const company = value => ({
  status: value.length > 0 ? 'ok' : 'fail',
  msg: 'Введите название компании',
});

export const name = value => ({
  status: value.length > 0 ? 'ok' : 'fail',
  msg: 'Введите имя',
});
