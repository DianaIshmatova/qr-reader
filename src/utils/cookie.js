export function getCookie(name) {
  if (typeof document === 'undefined') return null;
  const matches = document.cookie.match(new RegExp(
    `(?:^|; )${name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1')}=([^;]*)`, //eslint-disable-line
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
