const path = require('path');

module.exports = () => ({
  plugins: {
    'postcss-mixins': {
      mixinsDir: path.resolve(__dirname, './mixins'),
      mixins: require('./mixins/mixins'),
    },
    'postcss-import': {},
    'postcss-url': { url: 'inline' },
    'postcss-for': {},
    'postcss-cssnext': {
      browsers: ['last 2 versions'],
    },
  },
});
