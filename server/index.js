const path = require('path');
const express = require('express');
const app = express();
require('../build/all');

app.listen(app.get('port') || 9000, () => {
  console.log('Express server listening on port ' + (app.get('port') || 9000)); // eslint-disable-line
});

app.use(express.static(path.resolve(__dirname, '..', 'build')));
app.use(express.static(path.resolve(__dirname, 'static')));

const renderPage = (alias) => {
  /* eslint-disable */
  const React = require('react');
  const ReactDOMServer = require('react-dom/server');
  const template = `
      <!DOCTYPE html>
      <html>
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
        <title>ヽ(•‿•)ノ</title>
        <link rel="stylesheet" href="/${alias}.css">
        <link rel="stylesheet" href="/vendor.css">
        <link rel="stylesheet" href="/manifest.css">
      </head>
      <body>
        <div id="dev-root">${ReactDOMServer.renderToString(React.createElement(b2b[alias]))}</div>
        <script src="/manifest.js"></script>
        <script src="/vendor.js"></script>
        <script src="/${alias}.js"></script>
      </body>
      </html>
  `;
  return template;
};

app.get('/render/:alias', (req, res) => {
  console.log('Page: ', global.b2b);
  res.send(renderPage(req.params.alias));
});
