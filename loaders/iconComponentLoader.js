// Loader for Icon component from beeline-adaptive-components
// Made as svg export workaround for webpack version => 2.0
// TODO: Get rid of this loader!

module.exports = function iconComponentLoader(source) {
  this.cacheable();
  let result = source;
  if (this.context.indexOf('Footer') >= 0) {
    result = source.replace('sfsdf', '');
  } else {
    result = source.replace(
      "var Svg = require('./assets/' + name + '.svg');",
      "var Svg = require('./assets/' + name + '.svg').default;",
    );
  }
  return result;
};
