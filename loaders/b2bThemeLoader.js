const path = require('path');
const fs = require('fs');

module.exports = function b2bThemeLoader(source) {
  this.cacheable();

  if (this.context.indexOf('adaptive-components') >= 0) {
    const pathToTheme = path.resolve(this.context, 'themeB2b.pcss');

    try {
      const additionalSource = fs.readFileSync(pathToTheme, 'utf8');
      this.addDependency(pathToTheme);
      return additionalSource.startsWith('/* reset */') ? additionalSource : source + additionalSource;
    } catch (e) {
      // TODO: add error handler
    }
  }

  return source;
};
